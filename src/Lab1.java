// Lab1.java     STARTER FILE

import java.io.*; // I/O
import java.util.*; // Scanner class

public class Lab1
{
	public static void main( String args[] ) throws Exception
	{
		final double MILES_PER_MARATHON = 26.21875; // i.e 26 miles 285 yards

		Scanner kbd = new Scanner (System.in);
		double aveMPH=0, aveMinsPerMile=0, aveSecsPerMile=0; // YOU CALCULATE THESE BASED ON ABOVE INPUTS
		System.out.print("Enter marathon time in hrs minutes seconds: "); // i.e. 3 49 37
		// DO NOT WRITE OR MODIFY ANYTHING ABOVE THIS LINE
		
		double hours = 0, minutes = 0, seconds = 0, rawAve;
		
		if (kbd.hasNextInt()) {
			
			hours = kbd.nextInt();
			minutes = kbd.nextInt();
			seconds = kbd.nextInt();
		}
		
		rawAve = ((hours * 60) + minutes + (seconds / 60)) / MILES_PER_MARATHON;
		
		aveMPH = MILES_PER_MARATHON / (hours + (minutes / 60) + (seconds / 3600));
		aveMinsPerMile = Math.floor(rawAve);
		aveSecsPerMile = (rawAve - (int)rawAve) * 60;
		
		// DO NOT WRITE OR MODIFY ANYTHING BELOW THIS LINE. LET MY CODE PRINT THE VALUES YOU CALCULATED
		System.out.println();
		System.out.format("Average MPH was: %.2f mph\n", aveMPH  );
		System.out.format("Average mile split was %.0f mins %.1f seconds per mile", aveMinsPerMile, aveSecsPerMile );
		System.out.println();

	} // END MAIN METHOD
} // END LAB1 CLASS
